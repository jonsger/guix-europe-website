title: Minutes
date: 2022-07-09 0:00
---
## Finances

Our [financial
status](https://git.savannah.gnu.org/cgit/guix/maintenance.git/tree/guix-europe/accounting/accounting.ledger)
is public and permanently updated in a
[ledger](https://www.ledger-cli.org/) file.

## General Assemblies

The General Assembly usually takes place once a year by videoconference;
its minutes are public.

* [2022-07](/downloads/minutes/ga-20220703.txt)
* [2021](/downloads/minutes/ga-20211106.txt)
* [2020](/downloads/minutes/ga-20200621.txt)
* [2017](/downloads/minutes/ga-20171219.txt)


## Solidary Administrative Council

The SAC is the main decision taking body of Guix Europe. It is
currently composed of (...) shall we mention the members?
and publishes its minutes.

* 2022
  * [06-25](/downloads/minutes/sac-20220625.txt)
  * [06-23](/downloads/minutes/sac-20220623.txt)
* 2020
  * [06-12](/downloads/minutes/sac-20200612.txt)
  * [04-25](/downloads/minutes/sac-20200425.txt)
  * [01-28](/downloads/minutes/sac-20200128.txt)
  * [01-25](/downloads/minutes/sac-20200125.txt)
* 2019
  * [06-06](/downloads/minutes/sac-20190606.txt)
  * [06-03](/downloads/minutes/sac-20190603.txt)
  * [02-10](/downloads/minutes/sac-20190210.txt)
  * [01-28](/downloads/minutes/sac-20190128.txt)
  * [01-23](/downloads/minutes/sac-20190123.txt)
* 2018
  * [03-08](/downloads/minutes/sac-20180308.txt)
* 2017
  * [12-18](/downloads/minutes/sac-20171218.txt)
  * [11-16](/downloads/minutes/sac-20171116.txt)
  * [02-21](/downloads/minutes/sac-20170221.txt)
* 2016
  * [11-11](/downloads/minutes/sac-20161111.txt)
  * [06-02](/downloads/minutes/sac-20160602.txt)
  * [04-17](/downloads/minutes/sac-20160417.txt)
  * [03-10](/downloads/minutes/sac-20160310.txt)

