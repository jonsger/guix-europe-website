title: Foundation
date: 2022-07-09 0:00
---
Guix Europe is a non-profit association according to French law
(« loi 1901 »). Its aim is to provide a legal entity, independent of
any individuals working on the
[GNU Guix](https://guix.gnu.org/)
project, that can receive donations
(especially in Euro, complementing the
[FSF fundraising campaigns](https://my.fsf.org/civicrm/contribute/transact?reset=1&id=50)),
host activities and support the project in any conceivable way.

We own and host part of the infrastructure for the Guix build
farm, and helped fund and organise the Guix days around
[FOSDEM 2018](https://libreplanet.org/wiki/Group:Guix/FOSDEM2018),
[FOSDEM 2019](https://libreplanet.org/wiki/Group:Guix/FOSDEM2019),
[FOSDEM 2020](https://libreplanet.org/wiki/Group:Guix/FOSDEM2020)
and
[FOSDEM 2021](https://libreplanet.org/wiki/Group:Guix/FOSDEM2021).

The next planned event is the
[10 years of Guix](https://10years.guix.gnu.org/)
celebration in Paris.

The current board is composed of
[Simon Tournier](zimon.toutoune@gmail.com) as Presidency and
[Andreas Enge](andreas@enge.fr) as Treasury.
While well-identified public representatives of the association are
required by French law, our aim is to have a collegiate and
collective governance. In fact, all decisions are taken by the
Solidary Administrative Council, to which all members of the
association wishing to take responsability for its functioning
can be elected.

To know more about our bylaws, you may consult the
* [Statutes](/downloads/statutes/statuts-201602-en.pdf)
  (or their
  [French
  original](/downloads/statutes/statuts-201602-fr.pdf))
* [Interior
Reglementary](/downloads/statutes/interieur-201602-en.pdf)
  (or its
  [French
original](/downloads/statutes/interieur-201602-en.pdf))

If you wish to join, please fill in the
[membership
form](/downloads/statutes/membershipform.txt)
and send it to the e-mail addresses of the Presidency or the Treasury
given above.

