Minutes of the Solidary Administrative Council meeting of 2019-02-10


Present: Christopher Baines, Andreas Enge, Manolis Ragkousis, Ricardo Wurmus


1) Membership requests

The membership requests by Efraim Flashner, Julien Lepiller, Simon Tournier,
Matias Jose Seco Baccanelli, one other member and another potential member
(of whom no membership form is received yet) are accepted unanimously.

