Minutes of the Solidary Administrative Council meeting of 2020-01-28


Present: Ludovic Courtès, Andreas Enge, Manolis Ragkousis, Ricardo Wurmus


1) Supporting the Guix Days 2020 in Brussels

The following is decided unanimously:
- Guix Europe contributes 200€ towards the organisation of the Guix Days
  on January 30th and 31st 2020 in Brussels.
- Guix Europe advances the remaining costs of the Guix Days organisation,
  as agreed by the Guix spending committee.
- These remaining costs are to be reimbursed by the Guix funds held at
  the FSF.

This arrangement is also approved unanimously by the Guix spending committee.

