;;; Haunt website for Association France Vivipares
;;; Copyright © 2017, 2018, 2020, 2022 Andreas Enge <andreas@enge.fr>
;;;
;;; This is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (haunt site)
             (haunt reader)
             (haunt reader commonmark)
             (haunt builder assets)
             (builder static-site))

(define ge-sitemap
  (sitemap #:name "ge"
           #:title "Guix Europe"
           #:shorttitle "Guix Europe"
           #:chapters
           (list
             (chapter #:name "foundation" #:title "Foundation"
                      #:sections
                      (list
                        (section #:name "index"
                                 #:title "Introduction")))
             (chapter #:name "minutes" #:title "Minutes"
                      #:sections
                      (list
                        (section #:name "index"
                                 #:title "Minutes")))
             (chapter #:name "events" #:title "Events"
                      #:sections
                      (list
                        (section #:name "index"
                                 #:title "Events"))))))

(define ge-css '("bootstrap-5.1.3.css" "guix-europe.css"))

(define ge-js '("bootstrap-bundle-5.1.3.js"))

(define (file-filter name)
   "Ignore files ending with a ~, which are backup copies made by my editor,
as well as files ending with .swp."
   (not (or (eqv? (string-ref name (- (string-length name) 1)) #\~)
            (and (>= (string-length name) 4)
                 (string=? (string-take-right name 4) ".swp")))))

(site #:title "Guix Europe"
      #:domain "guix-europe.gnu.org"
      #:default-metadata
        '((author . "Andreas Enge")
          (email  . "andreas@enge.fr"))
      #:make-slug chapter-section-post-slug
      #:readers (list commonmark-reader html-reader)
      #:file-filter file-filter
      #:builders (list (static-site #:sitemap ge-sitemap
                                    #:language "en"
                                    #:logo "logo.jpg"
                                    #:css ge-css
                                    #:js ge-js)
                       (static-directory "assets")
                       (static-directory "downloads")))

